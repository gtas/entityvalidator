﻿using FluentValidation;
using ValidatorCore.Models;

namespace ValidatorCore.Validators
{
    public class SprinklerWaterSupplyValidator : AbstractValidator<SprinklerWaterSupplyModel>
    {
        public SprinklerWaterSupplyValidator()
        {
            RuleFor(model => model.SourceName).NotEmpty().NotNull().Must(sourceName =>
            {
                if (!string.IsNullOrWhiteSpace(sourceName))
                {
                    return sourceName.Length > 5;
                }

                return false;
            }).WithMessage("Source name should be more than 5 character!");
            RuleFor(model => model.Diameter).Must(diameter => diameter > 5).WithMessage("Diameter should be more than 5 in.");
        }
    }
}
