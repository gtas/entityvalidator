﻿using System;
using FluentValidation;
using ValidatorCore.Models;

namespace ValidatorCore.Validators
{
    public class SprinklerReportModelValidator : AbstractValidator<SprinklerReportModel>
    {
        public SprinklerReportModelValidator()
        {
            
            RuleFor(model => model.Name).NotEmpty().NotNull().Must((model, propertyValue) =>
            {
                if (!string.IsNullOrWhiteSpace(propertyValue))
                   return propertyValue.Length > 5;

                return true;
            });
            RuleFor(model => model.ScheduledDate).Must(BeAValidDateTime);
        }

        private bool BeAValidDateTime(DateTime date)
        {
            return date > new DateTime(2015, 1, 1);
        }
    }
}
