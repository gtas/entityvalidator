﻿using FluentValidation;
using ValidatorCore.Models;

namespace ValidatorCore.Validators
{
    public class SprinklerCommentValidator : AbstractValidator<SprinklerCommentsModel>
    {
        public SprinklerCommentValidator()
        {
            RuleFor(model => model.Comment).NotEmpty().NotNull().Must(comment =>
            {
                if (!string.IsNullOrWhiteSpace(comment))
                {
                    return comment.Length > 5;
                }

                return false;
            });
        }
    }
}
