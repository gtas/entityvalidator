﻿namespace ValidatorCore.Models
{
    public class ValidationErrorModel
    {
        public string PropertyName { get; set; }
        public string ValidationError { get; set; }

        public ValidationErrorModel(string propertyName, string validationError)
        {
            PropertyName = propertyName;
            ValidationError = validationError;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", PropertyName, ValidationError);
        }
    }
}
