﻿using System;
using System.Collections.ObjectModel;

namespace ValidatorCore.Models
{
    public class DataService
    {
        private static readonly SprinklerReportModel SprinklerReport = new SprinklerReportModel
        {
            WaterSupply = new SprinklerWaterSupplyModel
            {
                Diameter = 5,
                SourceName = "sou"
            },
            Comments = new ObservableCollection<SprinklerCommentsModel>
                {
                    new SprinklerCommentsModel {Comment = "GT"},
                    new SprinklerCommentsModel()
                },
            Name = "Geo",
            ScheduledDate = new DateTime(2014, 1, 1)
        };

        public SprinklerReportModel GetSprinklerReport()
        {
            return SprinklerReport;
        }
    }
}
