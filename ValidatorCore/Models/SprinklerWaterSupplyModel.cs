﻿using System.Threading.Tasks;
using FluentValidation.Results;
using ValidatorCore.Validators;

namespace ValidatorCore.Models
{
    public class SprinklerWaterSupplyModel : BaseModel
    {
        private readonly SprinklerWaterSupplyValidator _waterSupplyValidator = new SprinklerWaterSupplyValidator();
        
        private string _sourceName;
        private decimal _diameter;

        public string SourceName
        {
            get { return _sourceName; }
            set
            {
                _sourceName = value;
                OnPropertyChanged();
            }
        }

        public decimal Diameter
        {
            get { return _diameter; }
            set
            {
                _diameter = value;
                OnPropertyChanged();
            }
        }

        protected override Task<ValidationResult> GetValidationResultAsync()
        {
            return _waterSupplyValidator.ValidateAsync(this);
        }

        protected override ValidationResult GetValidationResult()
        {
            return _waterSupplyValidator.Validate(this);
        }
    }
}
