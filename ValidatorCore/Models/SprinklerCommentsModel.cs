﻿using System.Threading.Tasks;
using FluentValidation.Results;
using ValidatorCore.Validators;

namespace ValidatorCore.Models
{
    public class SprinklerCommentsModel : BaseModel
    {
        private readonly SprinklerCommentValidator _sprinklerCommentValidator = new SprinklerCommentValidator();

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                OnPropertyChanged();
            }
        }

        protected override Task<ValidationResult> GetValidationResultAsync()
        {
            return _sprinklerCommentValidator.ValidateAsync(this);
        }

        protected override ValidationResult GetValidationResult()
        {
            return _sprinklerCommentValidator.Validate(this);
        }
    }
}
