﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using FluentValidation.Results;
using ValidatorCore.Commands;
using ValidatorCore.Interfaces;

namespace ValidatorCore.Models
{
    public abstract class BaseModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        // Don't mind DI
        public static IDispatcher Dispatcher { get; set; }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region INotifyDataErrorInfo Related

        private IEnumerable<ValidationErrorModel> _validationErrors = new List<ValidationErrorModel>();
        public IEnumerable<ValidationErrorModel> ValidationErrors
        {
            get { return _validationErrors; }
            private set
            {
                _validationErrors = value;
                OnPropertyChanged();
            }
        }

        protected abstract Task<ValidationResult> GetValidationResultAsync();
        protected abstract ValidationResult GetValidationResult();

        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName) ||
                !ValidationErrors.Any())
                return null;

            return ValidationErrors.Where(validationError => validationError.PropertyName.Equals(propertyName));
        }

        public bool HasErrors
        {
            get
            {
                lock (ValidationErrors)
                {

                    ValidationErrors = Validate()
                        .Errors
                        .Select(validationFailure =>
                            new ValidationErrorModel(validationFailure.PropertyName, validationFailure.ErrorMessage));

                    return ValidationErrors.Any();
                }
            }
        }

        private RelayCommand _validateModelCommand;
        public RelayCommand ValidateModelCommand
        {
            get
            {
                return _validateModelCommand
                    ?? (_validateModelCommand = new RelayCommand(() => ValidateAsync()));
            }
        }

        public ValidationResult Validate()
        {
            return GetValidationResult();
        }

        public Task<ValidationResult> ValidateAsync()
        {
            Task<ValidationResult> validationResultTask = GetValidationResultAsync();

            validationResultTask.ContinueWith((antecedent) =>
            {
                if (antecedent.IsCompleted &&
                    !antecedent.IsCanceled &&
                    !antecedent.IsFaulted)
                {
                    ValidationResult validationResult = antecedent.Result;
                    if (validationResult != null &&
                        !validationResult.IsValid)
                    {
                        var errors = validationResult
                                        .Errors
                                        .GroupBy(p => p.PropertyName)
                                        .Select(p => p.First());

                        foreach (ValidationFailure validationFailure in errors)
                        {
                            RaiseErrorsChanged(validationFailure.PropertyName);
                        }
                    }
                }
            });
            return validationResultTask;
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged = delegate { };

        protected virtual void RaiseErrorsChanged(string propertyName)
        {
            var handler = ErrorsChanged;
            if (handler != null)
            {
                Dispatcher.InvokeOnMainThread(() =>
                {
                    handler(this, new DataErrorsChangedEventArgs(propertyName));
                });
            }
        }

        #endregion

    }
}
