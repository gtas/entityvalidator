﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FluentValidation.Results;
using ValidatorCore.Validators;

namespace ValidatorCore.Models
{
    public class SprinklerReportModel : BaseModel
    {
        private readonly SprinklerReportModelValidator _sprinklerReportValidator = new SprinklerReportModelValidator();
        
        private string _name;
        private DateTime _scheduledDate;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public DateTime ScheduledDate
        {
            get { return _scheduledDate; }
            set
            {
                _scheduledDate = value;
                OnPropertyChanged();
            }
        }

        public SprinklerWaterSupplyModel WaterSupply { get; set; }
        public ObservableCollection<SprinklerCommentsModel> Comments { get; set; }

        protected override Task<ValidationResult> GetValidationResultAsync()
        {
            return _sprinklerReportValidator.ValidateAsync(this);
        }

        protected override ValidationResult GetValidationResult()
        {
            return _sprinklerReportValidator.Validate(this);
        }
    }
}
