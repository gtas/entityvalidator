﻿using System;

namespace ValidatorCore.Interfaces
{
    public interface IDispatcher
    {
        void InvokeOnMainThread(Action action);
    }
}
