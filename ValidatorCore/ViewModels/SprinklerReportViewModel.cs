﻿using System.Threading.Tasks;
using ValidatorCore.Models;

namespace ValidatorCore.ViewModels
{
    public class SprinklerReportViewModel : BaseViewModel
    {
        private SprinklerReportModel _sprinklerReport;
        public SprinklerReportModel SprinklerReport
        {
            get { return _sprinklerReport; }
            set
            {
                _sprinklerReport = value;
                OnPropertyChanged();
            }
        }

        private SprinklerWaterSupplyViewModel _waterSupplyViewModel;
        public SprinklerWaterSupplyViewModel WaterSupplyViewModel
        {
            get { return _waterSupplyViewModel; }
            set
            {
                _waterSupplyViewModel = value;
                OnPropertyChanged();
            }
        }

        private SprinklerCommentsViewModel _commentsViewModel;
        public SprinklerCommentsViewModel CommentsViewModel
        {
            get { return _commentsViewModel; }
            set
            {
                _commentsViewModel = value;
                OnPropertyChanged();
            }
        }

        private int _selectedTabIndex = 0;
        public int SelectedTabIndex
        {
            get { return _selectedTabIndex; }
            set
            {
                _selectedTabIndex = value;
                OnPropertyChanged();
                ValidateOnSelectedTabIndex();
            }
        }

        private void ValidateOnSelectedTabIndex()
        {
            switch (SelectedTabIndex)
            {
                case 0:
                    WaterSupplyViewModel.ValidateModels();
                    CommentsViewModel.ValidateModels();
                    break;
                case 1:
                    ValidateModels();
                    CommentsViewModel.ValidateModels();
                    break;
                case 2:
                    ValidateModels();
                    WaterSupplyViewModel.ValidateModels();
                    break;
            }
        }

        public override Task ValidateModels()
        {
            return SprinklerReport.ValidateAsync();
        }

        public SprinklerReportViewModel()
        {
            WaterSupplyViewModel = new SprinklerWaterSupplyViewModel();
            CommentsViewModel = new SprinklerCommentsViewModel();
        }

        public Task Initialize(SprinklerReportModel sprinklerReport)
        {
            base.Initialize();

            SprinklerReport = sprinklerReport;
            WaterSupplyViewModel.Initialize(SprinklerReport.WaterSupply);
            CommentsViewModel.Initialize(SprinklerReport.Comments);

            return Task.FromResult(0);
        }
    }
}
