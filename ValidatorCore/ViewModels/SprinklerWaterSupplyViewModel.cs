﻿using System.Threading.Tasks;
using ValidatorCore.Models;

namespace ValidatorCore.ViewModels
{
    public class SprinklerWaterSupplyViewModel : BaseViewModel
    {
        private SprinklerWaterSupplyModel _sprinklerWaterSupply;

        public SprinklerWaterSupplyModel SprinklerWaterSupply
        {
            get { return _sprinklerWaterSupply; }
            set
            {
                _sprinklerWaterSupply = value;
                OnPropertyChanged();
            }
        }

        public Task Initialize(SprinklerWaterSupplyModel sprinklerWaterSupply)
        {
            base.Initialize();

            SprinklerWaterSupply = sprinklerWaterSupply;
            return Task.FromResult(0);
        }

        public override Task ValidateModels()
        {
            return SprinklerWaterSupply.ValidateAsync();
        }
    }
}
