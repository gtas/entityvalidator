﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ValidatorCore.Models;

namespace ValidatorCore.ViewModels
{
    public class SprinklerCommentsViewModel : BaseViewModel
    {
        private ObservableCollection<SprinklerCommentsModel> _sprinklerComments;

        public ObservableCollection<SprinklerCommentsModel> SprinklerComments
        {
            get { return _sprinklerComments; }
            set
            {
                _sprinklerComments = value;
                OnPropertyChanged();
            }
        }

        public Task Initialize(ObservableCollection<SprinklerCommentsModel> sprinklerComments)
        {
            base.Initialize();

            SprinklerComments = sprinklerComments;
            return Task.FromResult(0);
        }

        public override Task ValidateModels()
        {
            List<Task> tasks = new List<Task>();
            foreach (SprinklerCommentsModel sprinklerComment in SprinklerComments)
            {
                tasks.Add(sprinklerComment.ValidateAsync());
            }

            return Task.WhenAll(tasks);
        }
    }
}
