﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ValidatorCore.Commands;

namespace ValidatorCore.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual Task Initialize()
        {
            return Task.FromResult(0);
        }

        private RelayCommand _validateModelsCommand;
        public RelayCommand ValidateModelsCommand
        {
            get
            {
                return _validateModelsCommand
                       ?? (_validateModelsCommand = new RelayCommand(() => ValidateModels()));
            }
        }

        public abstract Task ValidateModels();
    }
}
