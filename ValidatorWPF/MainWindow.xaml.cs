﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidatorCore.Models;
using ValidatorCore.ViewModels;
using ValidatorWPF.Models;

namespace ValidatorWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SprinklerReportViewModel Vm { get; set; }
        public MainWindow()
        {
            InitializeComponent();

            BaseModel.Dispatcher = new DispatcherWpf(Application.Current.Dispatcher);
            Vm = new SprinklerReportViewModel();
            DataService dataService = new DataService();
            DataContext = Vm;
            Loaded += (sender, args) => Vm.Initialize(dataService.GetSprinklerReport());
        }

        private void Validation_OnError(object sender, ValidationErrorEventArgs e)
        {
            Debug.WriteLine("Main Window Validation Error: {0}", e.Error.ErrorContent);
        }
    }
}
