﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ValidatorWPF.UserControls
{
    /// <summary>
    /// Interaction logic for SprinklerCommentsUserControl.xaml
    /// </summary>
    public partial class SprinklerCommentsUserControl : UserControl
    {
        public SprinklerCommentsUserControl()
        {
            InitializeComponent();
        }

        private void Validation_OnError(object sender, ValidationErrorEventArgs e)
        {
            //Debug.WriteLine("Sprinkler Comments Validation Error: {0}", e.Error.ErrorContent);
        }
    }
}
