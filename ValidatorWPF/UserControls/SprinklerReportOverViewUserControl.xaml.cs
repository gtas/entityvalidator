﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ValidatorWPF.UserControls
{
    /// <summary>
    /// Interaction logic for SprinklerReportOverViewUserControl.xaml
    /// </summary>
    public partial class SprinklerReportOverViewUserControl : UserControl
    {
        public SprinklerReportOverViewUserControl()
        {
            InitializeComponent();
        }

        private void Validation_OnError(object sender, ValidationErrorEventArgs e)
        {
            //Debug.WriteLine("Sprinkler Report Overview Validation Error: {0}", e.Error.ErrorContent);
        }
    }
}
