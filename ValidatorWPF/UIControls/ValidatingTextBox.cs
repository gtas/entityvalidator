﻿using System.Windows;
using System.Windows.Controls;

namespace ValidatorWPF.UIControls
{
    public class ValidatingTextBox : TextBox
    {
        public static readonly DependencyProperty HasErrorsProperty =
            DependencyProperty.Register("HasErrors", typeof(bool), typeof(TextBox));

        public bool HasErrors
        {
            get
            {
                return (bool) this.GetValue(HasErrorsProperty);
            }
            set
            {
                this.SetValue(HasErrorsProperty, value);
            }
        }
    }
}
