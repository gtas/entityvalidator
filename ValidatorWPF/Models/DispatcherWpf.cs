﻿using System;
using System.Windows.Threading;
using ValidatorCore.Interfaces;

namespace ValidatorWPF.Models
{
    public class DispatcherWpf : IDispatcher
    {
        protected Dispatcher Dispatcher { get; private set; }

        public DispatcherWpf(Dispatcher dispatcher)
        {
            this.Dispatcher = dispatcher;
        }

        public void InvokeOnMainThread(Action action)
        {
            this.Dispatcher.BeginInvoke(action);
        }
    }
}
